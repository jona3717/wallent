#include "appointments.h"
#include "QString"
using namespace std;

Appointments::Appointments(){

}

void Appointments::setId(int id){
    _id = id;
}

int Appointments::getId(){
    return _id;
}

void Appointments::setClient(QString client){
    _client = client;
}

QString Appointments::getClient(){
    return _client;
}

void Appointments::setTime(QString time){
    _time = time;
}

QString Appointments::getTime(){
    return _time;
}

void Appointments::setDate(QString date){
    _date = date;
}

QString Appointments::getDate(){
    return _date;
}

void Appointments::setMotive(QString motive){
    _motive = motive;
}

QString Appointments::getMotive(){
    return _motive;
}

void Appointments::setLocation(QString location){
    _location = location;
}

QString Appointments::getLocation(){
    return _location;
}

void Appointments::setObservations(QString observations){
    _observations = observations;
}

QString Appointments::getObservations(){
    return _observations;
}
