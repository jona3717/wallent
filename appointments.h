#ifndef APPOINTMENTS_H
#define APPOINTMENTS_H
#include <QString>

class Appointments
{
public:
    Appointments();
    void setId(int);
    int getId();
    void setClient(QString);
    QString getClient();
    void setTime(QString);
    QString getTime();
    void setDate(QString);
    QString getDate();
    void setMotive(QString);
    QString getMotive();
    void setLocation(QString);
    QString getLocation();
    void setObservations(QString);
    QString getObservations();

private:
    int _id;
    QString _client;
    QString _time;
    QString _date;
    QString _motive;
    QString _location;
    QString _observations;
};

#endif // APPOINTMENTS_H
