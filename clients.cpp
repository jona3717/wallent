#include "clients.h"
#include "QString"
using namespace std;

Clients::Clients(){

}

void Clients::setName(QString name){
    _name = name;
}

QString Clients::getName(){
    return _name;
}

void Clients::setPaternalLastname(QString paternalLastName){
    _paternalLastname = paternalLastName;
}

QString Clients::getPaternalLastname(){
    return _paternalLastname;
}

void Clients::setMaternalLastname(QString maternalLastName){
    _maternalLastname = maternalLastName;
}

QString Clients::getMaternalLastname(){
    return _maternalLastname;
}

void Clients::setMobile(QString mobile){
    _mobile = mobile;
}

QString Clients::getMobile(){
    return _mobile;
}

void Clients::setEmail(QString email){
    _email = email;
}

QString Clients::getEmail(){
    return _email;
}

void Clients::setCompany(QString company){
    _company = company;
}

QString Clients::getCompany(){
    return _company;
}

void Clients::setPhone(QString phone){
    _phone = phone;
}

QString Clients::getPhone(){
    return _phone;
}

void Clients::setDocument(QString document){
    _document = document;
}

QString Clients::getDocument(){
    return _document;
}
