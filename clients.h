#ifndef CLIENTS_H
#define CLIENTS_H
#include <iostream>
#include "QString"

class Clients
{
public:
    Clients();
    void setName(QString);
    QString getName();
    void setPaternalLastname(QString);
    QString getPaternalLastname();
    void setMaternalLastname(QString);
    QString getMaternalLastname();
    void setMobile(QString);
    QString getMobile();
    void setEmail(QString);
    QString getEmail();
    void setCompany(QString);
    QString getCompany();
    void setPhone(QString);
    QString getPhone();
    void setDocument(QString);
    QString getDocument();

private:
    QString _name;
    QString _paternalLastname;
    QString _maternalLastname;
    QString _mobile;
    QString _email;
    QString _company;
    QString _phone;
    QString _document;
};

#endif // CLIENTS_H
