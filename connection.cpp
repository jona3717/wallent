#include "connection.h"
#include "clients.h"
#include "appointments.h"
#include "vector"
#include "QSqlDatabase"
#include "QSqlError"
#include "QSqlQuery"
#include "QDebug"
using namespace std;

Connection::Connection(){
    createTables();
}

void Connection::connect(){
    QSqlDatabase bd = QSqlDatabase::addDatabase("QSQLITE");
    bd.setDatabaseName("wallent.db");

    if (!bd.open()){
        qDebug() << bd.lastError().text();
        return;
    } else{
        _bd = bd;
    }
}

void Connection::sendQuery(QString value){
    connect();
    QSqlQuery query;
    if (!query.exec(value)){
        qDebug() << "Error:";
        qDebug() << query.lastError().text();
        return;
    } else
        _query = query;
}

void Connection::createTables(){
    QString query = "CREATE TABLE IF NOT EXISTS people(name TEXT, paternal_lastname TEXT, maternal_lastname TEXT, "
                    "mobile TEXT, email TEXT, company TEXT, phone TEXT,  document TEXT NOT NULL PRIMARY KEY)";
    sendQuery(query);

    query = "CREATE TABLE IF NOT EXISTS appointments(id INTEGER PRIMARY KEY AUTOINCREMENT, "
            "id_client TEXT REFERENCES people, time TEXT, date TEXT, motive TEXT, location TEXT, "
            "observations TEXT, FOREIGN KEY(id_client) REFERENCES people)";
    sendQuery(query);
    _bd.close();
}

void Connection::newPeople(Clients client){
    QString query = ("INSERT INTO people VALUES('" + client.getName() + "', '" + client.getPaternalLastname() + "', '" +
                     client.getMaternalLastname() + "', '" + client.getMobile() + "', '" + client.getEmail() + "', '" +
                     client.getCompany() + "', '" + client.getPhone() + "', '" + client.getDocument() + "')");

    sendQuery(query);
    _bd.close();
}

void Connection::newAppointment(Appointments appointment){
    QString query = ("INSERT INTO appointments VALUES(null, '" + appointment.getClient() + "', '" +
                     appointment.getTime() + "', '" + appointment.getDate() + "', '" + appointment.getMotive() + "', '" +
                     appointment.getLocation() + "', '" + appointment.getObservations() + "')");

    sendQuery(query);
    _bd.close();
}

QVector<Clients> Connection::loadClients(){
    QVector<Clients> clients;
    QString query;

    query = ("SELECT * FROM people");
    sendQuery(query);

    while (_query.next()) {
        QString name = _query.value("name").toString();
        QString paternalLastname = _query.value("paternal_lastname").toString();
        QString maternalLastname = _query.value("maternal_lastname").toString();
        QString mobile = _query.value("mobile").toString();
        QString email = _query.value("email").toString();
        QString company = _query.value("company").toString();
        QString phone = _query.value("phone").toString();
        QString document = _query.value("document").toString();

        Clients client;
        client.setName(name);
        client.setPaternalLastname(paternalLastname);
        client.setMaternalLastname(maternalLastname);
        client.setMobile(mobile);
        client.setEmail(email);
        client.setCompany(company);
        client.setPhone(phone);
        client.setDocument(document);

        clients.append(client);
    }
    _bd.close();

    return clients;
}

QVector<Appointments> Connection::loadAppointments(){
    QVector<Appointments> appointments;
    QString query;

    query = ("SELECT * FROM appointments");
    sendQuery(query);

    while (_query.next()) {
        int id = _query.value("id").toInt();
        QString client = _query.value("id_client").toString();
        QString time = _query.value("time").toString();
        QString date = _query.value("date").toString();
        QString motive = _query.value("motive").toString();
        QString location = _query.value("location").toString();
        QString observations = _query.value("observations").toString();

        Appointments appointment;
        appointment.setId(id);
        appointment.setClient(client);
        appointment.setTime(time);
        appointment.setDate(date);
        appointment.setMotive(motive);
        appointment.setLocation(location);
        appointment.setObservations(observations);

        appointments.append(appointment);
    }
    _bd.close();

    return appointments;
}

void Connection::deleteClient(Clients client){
    QString query;

    query = "DELETE FROM people WHERE document LIKE '" + client.getDocument() + "'";
    sendQuery(query);
    _bd.close();
}

void Connection::deleteAppointment(Appointments appointment){
    QString query;

    query = "DELETE FROM people WHERE document LIKE '" + QString(appointment.getId()) + "'";
    sendQuery(query);
    _bd.close();
}

QVector<Clients> Connection::searchClient(QString name){
    QVector<Clients> clients;
    QString query;

    query = ("SELECT * FROM people WHERE name LIKE '%" + name + "%'");
    sendQuery(query);

    while (_query.next()) {
        QString name = _query.value("name").toString();
        QString paternalLastname = _query.value("paternal_lastname").toString();
        QString maternalLastname = _query.value("maternal_lastname").toString();
        QString mobile = _query.value("mobile").toString();
        QString email = _query.value("email").toString();
        QString company = _query.value("company").toString();
        QString phone = _query.value("phone").toString();
        QString document = _query.value("document").toString();

        Clients client;
        client.setName(name);
        client.setPaternalLastname(paternalLastname);
        client.setMaternalLastname(maternalLastname);
        client.setMobile(mobile);
        client.setEmail(email);
        client.setCompany(company);
        client.setPhone(phone);
        client.setDocument(document);

        clients.append(client);
    }
    _bd.close();

    return clients;
}

QVector<Appointments> Connection::searchAppointment(QString name){
    QVector<Appointments> appointments;
    QString query;

    query = ("SELECT id, id_client, time, date, motive, location, observations, name "
             "FROM appointments INNER JOIN people on people.document = appointments.id_client "
             "WHERE name like '%" + name + "%'");
    sendQuery(query);

    while (_query.next()) {
        int id = _query.value("id").toInt();
        QString client = _query.value("id_client").toString();
        QString time = _query.value("time").toString();
        QString date = _query.value("date").toString();
        QString motive = _query.value("motive").toString();
        QString location = _query.value("location").toString();
        QString observations = _query.value("observations").toString();

        Appointments appointment;
        appointment.setId(id);
        appointment.setClient(client);
        appointment.setTime(time);
        appointment.setDate(date);
        appointment.setMotive(motive);
        appointment.setLocation(location);
        appointment.setObservations(observations);

        appointments.append(appointment);
    }
    _bd.close();

    return appointments;
}
