#ifndef CONNECTION_H
#define CONNECTION_H
#include "QSqlDatabase"
#include "QSqlQuery"
#include "clients.h"
#include "appointments.h"

class Connection
{
public:
    Connection();
    void newPeople(Clients);
    void newAppointment(Appointments);
    QVector<Clients> loadClients();
    QVector<Appointments> loadAppointments();
    void deleteClient(Clients);
    void deleteAppointment(Appointments);
    QVector<Clients> searchClient(QString);
    QVector<Appointments> searchAppointment(QString);

private:
    QSqlDatabase _bd;
    QSqlQuery _query;

    void connect();
    void sendQuery(QString);
    void createTables();
};

#endif // CONNECTION_H
