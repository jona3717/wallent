#include "newappointment.h"
#include "ui_newappointment.h"
#include "connection.h"
#include "QMessageBox"

NewAppointment::NewAppointment(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewAppointment)
{
    ui->setupUi(this);
    loadClients();
    ui->cbxClient->setFocus();
}

NewAppointment::~NewAppointment()
{
    delete ui;
}

void NewAppointment::on_btnAccept_clicked(){
    if (NewAppointment::verify()){
        Appointments appointment;
        Connection connect;

        int index = ui->cbxClient->currentIndex();

        QString client = clients[index].getDocument();
        QString time = ui->timeEdit->text();
        QString date = ui->dateEdit->text();
        QString motive = ui->txtMotive->text();
        QString location = ui->txtLocation->text();
        QString observations = ui->txtObservations->toPlainText();

        appointment.setClient(client);
        appointment.setTime(time);
        appointment.setDate(date);
        appointment.setMotive(motive);
        appointment.setLocation(location);
        appointment.setObservations(observations);

        connect.newAppointment(appointment);
        cleanFields();
        QMessageBox::information(this, "Registrada", "Registro exitoso.");
    }
}

void NewAppointment::on_btnCancel_clicked(){
    close();
}

void NewAppointment::cleanFields(){
    QTime time = QTime::currentTime();
    QDate date = QDate::currentDate();

    ui->cbxClient->setCurrentIndex(0);
    ui->timeEdit->setTime(time);
    ui->dateEdit->setDate(date);
    ui->txtMotive->setText("");
    ui->txtLocation->setText("");
    ui->txtObservations->clear();

    ui->cbxClient->setFocus();
}

void NewAppointment::loadClients(){
    Connection connect;
    clients = connect.loadClients();

    for(int i=0; i < clients.size(); i++){
        QString fullname = clients[i].getName() + " " +
                clients[i].getPaternalLastname() + " " +
                clients[i].getMaternalLastname();
        ui->cbxClient->addItem(fullname);
    }
}

bool NewAppointment::verify(){
    bool value = true;
    QString msj;

    if (ui->txtMotive->text().size() == 0){
        msj += "El campo motivo está vacío.\n";
    }
    if (ui->txtLocation->text().size() < 2){
        msj += "Debes ingresar una dirección válida.";
    }

    if (msj.size() > 0){
        value = false;
        QMessageBox::warning(this, "Error", msj);
    }
    return value;
}
