#ifndef NEWAPPOINTMENT_H
#define NEWAPPOINTMENT_H

#include <QDialog>
#include "clients.h"

namespace Ui {
class NewAppointment;
}

class NewAppointment : public QDialog
{
    Q_OBJECT

public:
    explicit NewAppointment(QWidget *parent = nullptr);
    ~NewAppointment();

private:
    Ui::NewAppointment *ui;
    void cleanFields();
    void loadClients();
    QVector<Clients> clients;
    bool verify();

private slots:
    void on_btnAccept_clicked();
    void on_btnCancel_clicked();
};

#endif // NEWAPPOINTMENT_H
