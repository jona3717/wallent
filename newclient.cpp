#include "newclient.h"
#include "ui_newclient.h"
#include "clients.h"
#include "connection.h"
#include "QMessageBox"

NewClient::NewClient(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewClient)
{
    ui->setupUi(this);
    ui->txtName->setFocus();
}

NewClient::~NewClient()
{
    delete ui;
}

void NewClient::on_btnAccept_clicked(){
    if (NewClient::verify()){
        Connection connection;
        Clients client;

        client.setName(ui->txtName->text());
        client.setPaternalLastname(ui->txtPaternLastname->text());
        client.setMaternalLastname((ui->txtMaternLastname->text()));
        client.setMobile(ui->txtMobile->text());
        client.setEmail(ui->txtEmail->text());
        client.setCompany(ui->txtCompany->text());
        client.setPhone(ui->txtPhone->text());
        client.setDocument(ui->txtDocument->text());

        connection.newPeople(client);

        cleanFields();
        QMessageBox::information(this, "Registrado", "Registro exitoso.");
    }
}

void NewClient::on_btnCancel_clicked(){
    close();
}

void NewClient::cleanFields(){
    ui->txtName->setText("");
    ui->txtPaternLastname->setText("");
    ui->txtMaternLastname->setText("");
    ui->txtEmail->setText("");
    ui->txtMobile->setText("");
    ui->txtCompany->setText("");
    ui->txtPhone->setText("");
    ui->txtDocument->setText("");

    ui->txtName->setFocus();
}

bool NewClient::verify(){
    bool value = true;
    QString msj;
    if (ui->txtName->text().size() < 2){
        msj += "Por favor ingresa un nombre válido.\n";
    }
    if (ui->txtPaternLastname->text().size() < 2){
        msj += "Por favor ingresa un apellido paterno válido.\n";
    }
    if (ui->txtMaternLastname->text().size() < 2){
        msj += "Por favor ingresa un apellido materno válido.\n";
    }
    if (ui->txtEmail->text().size() < 8){
        msj += "Por favor ingresa una dirección de e-mail válida.";
    }

    if (msj.size() > 0){
        value = false;
        QMessageBox::warning(this, "Error", msj);
    }

    return value;
}
