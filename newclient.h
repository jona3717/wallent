#ifndef NEWCLIENT_H
#define NEWCLIENT_H

#include <QDialog>

namespace Ui {
class NewClient;
}

class NewClient : public QDialog
{
    Q_OBJECT

public:
    explicit NewClient(QWidget *parent = nullptr);
    ~NewClient();

private:
    Ui::NewClient *ui;

    void cleanFields();
    bool verify();

private slots:
    void on_btnAccept_clicked();
    void on_btnCancel_clicked();
};

#endif // NEWCLIENT_H
