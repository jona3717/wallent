#include "showappointment.h"
#include "ui_showappointment.h"

ShowAppointment::ShowAppointment(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShowAppointment)
{
    ui->setupUi(this);
}

ShowAppointment::~ShowAppointment()
{
    delete ui;
}

void ShowAppointment::loadAppointment(QString client){
    ui->lblName->setText(client);
    ui->lblTime->setText(appointment.getTime());
    ui->lblDate->setText(appointment.getDate());
    ui->lblMotive->setText(appointment.getMotive());
    ui->lblLocation->setText(appointment.getLocation());
    ui->lblObservations->setText(appointment.getObservations());
}

void ShowAppointment::on_btnAccept_clicked(){
    close();
}

void ShowAppointment::on_btnEdit_clicked(){

}
