#ifndef SHOWAPPOINTMENT_H
#define SHOWAPPOINTMENT_H

#include <QDialog>
#include "appointments.h"

namespace Ui {
class ShowAppointment;
}

class ShowAppointment : public QDialog
{
    Q_OBJECT

public:
    explicit ShowAppointment(QWidget *parent = nullptr);
    ~ShowAppointment();
    Appointments appointment;
    void loadAppointment(QString);

private:
    Ui::ShowAppointment *ui;

private slots:
    void on_btnAccept_clicked();
    void on_btnEdit_clicked();
};

#endif // SHOWAPPOINTMENT_H
