#include "showclient.h"
#include "ui_showclient.h"
#include "clients.h"

ShowClient::ShowClient(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ShowClient){
    ui->setupUi(this);
}

ShowClient::~ShowClient(){
    delete ui;
}

void ShowClient::loadClient(){
    QString fullname = client.getName() + " " +
            client.getPaternalLastname() + " " +
            client.getMaternalLastname();

    ui->lblName->setText(fullname);
    ui->lblMobile->setText(client.getMobile());
    ui->lblEmail->setText(client.getEmail());
    ui->lblCompany->setText(client.getCompany());
    ui->lblPhone->setText(client.getPhone());
    ui->lblDocument->setText(client.getDocument());
}

void ShowClient::on_btnAccept_clicked(){
    close();
}

void ShowClient::on_btnEdit_clicked(){

}
