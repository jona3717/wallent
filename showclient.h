#ifndef SHOWCLIENT_H
#define SHOWCLIENT_H

#include <QDialog>
#include "clients.h"

namespace Ui {
class ShowClient;
}

class ShowClient : public QDialog
{
    Q_OBJECT

public:
    explicit ShowClient(QWidget *parent = nullptr);
    ~ShowClient();
    Clients client;
    void loadClient();

private:
    Ui::ShowClient *ui;

private slots:
    void on_btnAccept_clicked();
    void on_btnEdit_clicked();
};

#endif // SHOWCLIENT_H
