#include "wallent.h"
#include "./ui_wallent.h"
#include "clients.h"
#include "appointments.h"
#include "newclient.h"
#include "newappointment.h"
#include "connection.h"
#include "showclient.h"
#include "showappointment.h"
#include "QMessageBox"
#include "QDebug"
#include "QKeyEvent"

Wallent::Wallent(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Wallent)
{
    ui->setupUi(this);

    qApp->installEventFilter(this);

    QIcon clientsIcon("./resources/clients.png");
    QIcon appointmentIcon("./resources/appointments.png");

    QIcon deleteIcon("./resources/delete.png");
    QIcon eyeOpenedIcon("./resources/eye_opened.png");
    QIcon eyeClosedIcon("./resources/eye_closed.png");

    QIcon newClientIcon("./resources/new_client.png");
    QIcon newAppointmentIcon("./resources/new_appointment.png");
    QIcon searchClientIcon("./resources/search_client.png");
    QIcon searchAppointmentIcon("./resources/search_appointment.png");

    ui->btnClients->setIcon(clientsIcon);
    ui->btnAppointments->setIcon(appointmentIcon);

    ui->btnNewClient->setIcon(newClientIcon);
    ui->btnNewAppointment->setIcon(newAppointmentIcon);
    ui->btnDeleteClient->setIcon(deleteIcon);
    ui->btnDeleteAppointment->setIcon(deleteIcon);
    ui->btnShowClient->setIcon(eyeClosedIcon);
    ui->btnShowAppointment->setIcon(eyeClosedIcon);
    ui->btnSearchClient->setIcon(searchClientIcon);
    ui->btnSearchAppointment->setIcon(searchAppointmentIcon);

    //Confuguración de tablas
    ui->tblClients->setColumnCount(2);
    ui->tblClients->setRowCount(0);
    ui->tblAppointments->setColumnCount(2);
    ui->tblAppointments->setRowCount(0);
    //Evita edición de las celdas
    ui->tblClients->setEditTriggers(QAbstractItemView::NoEditTriggers);
    ui->tblAppointments->setEditTriggers(QAbstractItemView::NoEditTriggers);
    //Selecciona toda la fila
    ui->tblClients->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->tblAppointments->setSelectionBehavior(QAbstractItemView::SelectRows);
    //Se selecciona una sola fila a la vez
    ui->tblClients->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->tblAppointments->setSelectionMode(QAbstractItemView::SingleSelection);
    //Dibujar el fondo usando colores alternados
    ui->tblAppointments->setAlternatingRowColors(true);
    ui->tblClients->setAlternatingRowColors(true);
    //Coloca puntos suspensivos a la derecha si el texto
    //excede el ancho de la celda
    ui->tblClients->setTextElideMode(Qt::ElideRight);
    ui->tblAppointments->setTextElideMode(Qt::ElideRight);
    //Establece el nombre de las columnas
    QStringList clientsColumns = {"Cliente", "Empresa"};
    QStringList appointmentsColumns = {"Hora", "Cliente"};
    ui->tblClients->setHorizontalHeaderLabels(clientsColumns);
    ui->tblAppointments->setHorizontalHeaderLabels(appointmentsColumns);
    //Ajustar ancho de última columna al espacio restante
    ui->tblClients->horizontalHeader()->setStretchLastSection(true);
    ui->tblAppointments->horizontalHeader()->setStretchLastSection(true);

    activeClients(false);
    activeAppointments(false);

    loadClients();
    loadAppointments();
}

Wallent::~Wallent()
{
    delete ui;
}

void Wallent::on_btnClients_clicked(){
    activeClients(true);
    activeAppointments(false);
}

void Wallent::on_btnAppointments_clicked(){
    activeClients(false);
    activeAppointments(true);
}


void Wallent::on_btnNewAppointment_clicked(){
    NewAppointment form;
    form.exec();
    loadAppointments();
    viewAppointments();
}

void Wallent::on_btnNewClient_clicked(){
    NewClient form;
    form.exec();
    loadClients();
    viewClients();
}

void Wallent::on_btnShowClient_clicked(){
    int selection = ui->tblClients->currentRow();
    if(selection >= 0)
        showClient();
    else
        QMessageBox::warning(this, "Error", "Debes seleccionar un cliente.");
}

void Wallent::on_btnShowAppointment_clicked(){
    int selection = ui->tblAppointments->currentRow();
    if(selection >= 0)
        showAppointment();
    else
        QMessageBox::warning(this, "Error", "Debes seleccionar una cita para poder verla.");
}

void Wallent::on_btnDeleteClient_clicked(){
    int selection = ui->tblClients->currentRow();
    if(selection >= 0)
        deleteClient();
    else
        QMessageBox::warning(this, "Error", "Selecciona el cliente que deseas eliminar");
}

void Wallent::on_btnDeleteAppointment_clicked(){
    int selection = ui->tblAppointments->currentRow();
    if(selection >= 0)
        deleteAppointment();
    else
        QMessageBox::warning(this, "Error", "Selecciona la cita que deseas eliminar.");
}

void Wallent::on_btnSearchClient_clicked(){
    QString client = ui->txtSearchClient->text();
    if(client.size() > 0)
        searchClient(client);
    else
        QMessageBox::warning(this, "Error", "Nada que buscar");
}

void Wallent::on_btnSearchAppointment_clicked(){
    QString appointment = ui->txtSearchAppointment->text();
    if(appointment.size() > 0)
        searchAppointment(appointment);
    else
        QMessageBox::warning(this, "Error", "Nada que buscar.");
}

void Wallent::activeClients(bool value){
    if (value) {
        ui->btnNewClient->show();
        ui->btnDeleteClient->show();
        ui->btnShowClient->show();
        ui->txtSearchClient->show();
        ui->btnSearchClient->show();
        ui->tblClients->show();

        ui->btnNewAppointment->hide();
        ui->btnDeleteAppointment->hide();
        ui->btnShowAppointment->hide();
        ui->txtSearchAppointment->hide();
        ui->btnSearchAppointment->hide();
        ui->tblAppointments->hide();

        viewClients();

        ui->txtSearchClient->setFocus();
    }else {
        ui->btnNewClient->hide();
        ui->btnDeleteClient->hide();
        ui->btnShowClient->hide();
        ui->txtSearchClient->hide();
        ui->btnSearchClient->hide();
        ui->tblClients->hide();
    }

}

void Wallent::activeAppointments(bool value){
    if (value) {
        ui->btnNewClient->hide();
        ui->btnDeleteClient->hide();
        ui->btnShowClient->hide();
        ui->txtSearchClient->hide();
        ui->btnSearchClient->hide();
        ui->tblClients->hide();

        ui->btnNewAppointment->show();
        ui->btnDeleteAppointment->show();
        ui->btnShowAppointment->show();
        ui->txtSearchAppointment->show();
        ui->btnSearchAppointment->show();
        ui->tblAppointments->show();

        viewAppointments();

        ui->txtSearchAppointment->setFocus();
    }else {
        ui->btnNewAppointment->hide();
        ui->btnDeleteAppointment->hide();
        ui->btnShowAppointment->hide();
        ui->txtSearchAppointment->hide();
        ui->btnSearchAppointment->hide();
        ui->tblAppointments->hide();
    }
}

void Wallent::loadClients(){
    Connection connect;
    clients = connect.loadClients();
}

void Wallent::loadAppointments(){
    Connection connect;
    appointments = connect.loadAppointments();
}

void Wallent::viewClients(){
    ui->tblClients->clearContents();

    int row = 0;

    for (Clients client : clients){
        ui->tblClients->setRowCount(row + 1);
        QString fullname = client.getName() + " " +
                client.getPaternalLastname() + " " +
                client.getMaternalLastname();
        QTableWidgetItem *item = new QTableWidgetItem(fullname);
        ui->tblClients->setItem(row, 0, item);
        item = new QTableWidgetItem(client.getCompany());
        ui->tblClients->setItem(row, 1, item);

        row += 1;
    }

    ui->tblClients->resizeColumnToContents(0);
    ui->tblClients->horizontalHeader()->setStretchLastSection(true);
}

void Wallent::viewAppointments(){
    ui->tblAppointments->clearContents();

    int row = 0;

    for (Appointments appointment : appointments) {
        ui->tblAppointments->setRowCount(row + 1);
        QString name;
        for (Clients client : clients) {
            if (client.getDocument() == appointment.getClient()){
                name = client.getName() + " " +
                        client.getPaternalLastname() + " " +
                        client.getMaternalLastname();
                break;
            }
        }
        QTableWidgetItem *item = new QTableWidgetItem(appointment.getTime());
        ui->tblAppointments->setItem(row, 0, item);
        item = new QTableWidgetItem(name);
        ui->tblAppointments->setItem(row, 1, item);

        row += 1;
    }

    ui->tblClients->resizeColumnToContents(0);
    ui->tblClients->horizontalHeader()->setStretchLastSection(true);
}

void Wallent::deleteClient(){
    Connection connect;
    int selection = ui->tblClients->currentRow();

    connect.deleteClient(clients[selection]);
    ui->tblClients->removeRow(selection);
    clients.remove(selection);
}

void Wallent::deleteAppointment(){
    Connection connect;
    int selection = ui->tblAppointments->currentRow();

    connect.deleteAppointment(appointments[selection]);
    ui->tblAppointments->removeRow(selection);
    clients.remove(selection);
}

void Wallent::showClient(){
    int selection = ui->tblClients->currentRow();

    Clients client = clients[selection];
    ShowClient dialog;
    dialog.client = client;
    dialog.loadClient();
    dialog.exec();

}

void Wallent::showAppointment(){
    int selection = ui->tblAppointments->currentRow();
    QString clientName;

    for(Clients client : clients){
        if(appointments[selection].getClient() == client.getDocument()){
            clientName = client.getName() + " " +
                    client.getPaternalLastname() + " " +
                    client.getMaternalLastname();
            break;
        }
    }

    Appointments appointment = appointments[selection];
    ShowAppointment dialog;
    dialog.appointment = appointment;
    dialog.loadAppointment(clientName);
    dialog.exec();
}

void Wallent::searchClient(QString client){
    if (client.size() > 0){
        Connection con;
        clients = con.searchClient(client);
        viewClients();
    } else{
            loadClients();
            viewClients();
        }
}

void Wallent::searchAppointment(QString appointment){
    if (appointment.size() > 0){
        Connection con;
        appointments = con.searchAppointment(appointment);
        viewAppointments();
    } else{
        loadAppointments();
        viewAppointments();
    }
}

bool Wallent::eventFilter(QObject *object, QEvent *event){
    QString text = "";
    if(object == ui->txtSearchClient && event->type() == QEvent::KeyPress){
        QKeyEvent *key = static_cast<QKeyEvent *>(event);
                qDebug() << "pressed"<< key->text();
                if(key->key() == Qt::Key_Escape){
                    ui->txtSearchClient->setText("");
                    text = "";
                    loadClients();
                    viewClients();
                } else if(key->text() == "\r"){
                    ui->btnSearchClient->click();
                } else if(key->key() != Qt::Key_Backspace){
                    if(key->text().size() != 0){
                        text += key->text();
                        searchClient(text);
                    }
                }else{
                    if(ui->txtSearchClient->text().size() != 0){
                        searchClient(ui->txtSearchClient->text());
                    } else{
                        text = "";
                        loadClients();
                        viewClients();
                    }
                }
    } else if(object == ui->txtSearchAppointment && event->type() == QEvent::KeyPress){
        QKeyEvent *key = static_cast<QKeyEvent *>(event);
                qDebug() << "pressed"<< key->text();
                if(key->key() == Qt::Key_Escape){
                    ui->txtSearchAppointment->setText("");
                    text = "";
                    loadAppointments();
                    viewAppointments();
                } else if(key->text() == "\r"){
                    ui->btnSearchAppointment->click();
                } else if(key->key() != Qt::Key_Backspace){
                    if(key->text().size() != 0){
                        text += key->text();
                        searchAppointment(text);
                    }
                }else{
                    if(ui->txtSearchAppointment->text().size() != 0){
                        searchAppointment(ui->txtSearchAppointment->text());
                    } else{
                        text = "";
                        loadAppointments();
                        viewAppointments();
                    }
                }
    }
    return QObject::eventFilter(object, event);
}
