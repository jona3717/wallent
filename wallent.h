#ifndef WALLENT_H
#define WALLENT_H

#include <QMainWindow>
#include "clients.h"
#include "appointments.h"
#include "QLineEdit"

QT_BEGIN_NAMESPACE
namespace Ui { class Wallent; }
QT_END_NAMESPACE

class Wallent : public QMainWindow
{
    Q_OBJECT

public:
    Wallent(QWidget *parent = nullptr);
    ~Wallent();

    void activeClients(bool);
    void activeAppointments(bool);
    void loadClients();
    void viewClients();
    void loadAppointments();
    void viewAppointments();
    void deleteClient();
    void deleteAppointment();
    void showClient();
    void showAppointment();
    void searchClient(QString);
    void searchAppointment(QString);

private slots:
    void on_btnClients_clicked();
    void on_btnAppointments_clicked();

    void on_btnNewClient_clicked();
    void on_btnNewAppointment_clicked();
    void on_btnShowClient_clicked();
    void on_btnShowAppointment_clicked();
    void on_btnDeleteClient_clicked();
    void on_btnDeleteAppointment_clicked();
    void on_btnSearchClient_clicked();
    void on_btnSearchAppointment_clicked();

protected:
    bool eventFilter(QObject *object, QEvent *event);

private:
    Ui::Wallent *ui;

    QVector<Clients> clients;
    QVector<Appointments> appointments;
};
#endif // WALLENT_H
